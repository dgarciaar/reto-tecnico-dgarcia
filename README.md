Como siguiente etapa tenemos el reto tecnológico, en el cual evaluaremos tu perfil técnico.

El plazo de entrega es Viernes 20/11 al mediodía, por favor enviarnos el enlace del repositorio GitHub para poder revisarlo.
 
Descripción del reto técnico:

Crear una API en Node.js con el framework Serverless para un despliegue en AWS.
Adaptar y transformar los modelos de la API de prueba. Se tienen que mapear todos los nombres de atributos modelos del inglés al español (Ej: name -> nombre).
Integrar la API de prueba Star Wars API (líneas abajo está el link) se deben integrar uno o más endpoints.
Crear un modelo de su elección mediante el uso de un endpoint POST, la data se tendrá que almacenar dentro de una base de datos.
Crear un endpoint GET que muestre la data almacenada.

API de prueba SWAPI:

https://swapi.py4e.com/documentation

Puntos de evaluación:
Mínimo 2 endpoints, GET para recuperar la información y POST para crear un elemento
Integración con una base de datos (DynamoDB o MySQL)
Uso de Serverless Framework
Uso de Node.js
Respeto de las buenas prácticas de desarrollo
Puntos bonus:

Documentación de uso (2 puntos)
Pruebas unitarias (10 puntos)
Documentación en Open API/Swagger (2 puntos)
Desplegar sin errores en AWS con el comando deploy del framework serverless (2 puntos)
Mayor complejidad de Integración (3 puntos)

# DEV 
choco install serverless
serverless
serverless config credentials --provider provider --key key --secret secret
serverless deploy

Service Information
service: reto-rimac
stage: dev
region: us-east-1
stack: reto-rimac-dev
resources: 18
api keys:
  None
endpoints:
  POST - https://hh799191vh.execute-api.us-east-1.amazonaws.com/dev/sw/create
  GET - https://hh799191vh.execute-api.us-east-1.amazonaws.com/dev/sw/read
functions:
  sw_create: reto-rimac-dev-sw_create
  sw_read: reto-rimac-dev-sw_read
layers:
  None
  
cd .\reto-rimac\
serverless invoke local --function sw_read --path samples/sw_read.json